require('dotenv').config();
//librerias
const express=require('express');
const app=express();
//y por si acaso...
const bodyParser  = require('body-parser');

app.use(express.json());

var enableCORS = function(req, res, next) {
// No producción!!!11!!!11one!!1!
res.set("Access-Control-Allow-Origin", "*");
res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
res.set("Access-Control-Allow-Headers", "Content-Type");

next();
}
app.use(enableCORS);


//const io =require('./io');
const userController = require('./controllers/UserController');
const movementController = require('./controllers/MovementController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');

const port=process.env.PORT ||3000;

app.listen(port);
console.log("API escuchando en el puerto " + port);

//de prueba para los test
app.get("/apitechu/v1/hello",
function(req,res){
  console.log("GET /apitechu/v1/hello");
  res.send({"msg":"Hola desde API TechU!"});
}
)

//Consulta usuario
app.get("/lowcostbank/v1/users/:id", userController.getUsersById);
//Consulta usuarios
app.get("/lowcostbank/v1/users", userController.getUsers);
//Alta usuario
app.post("/lowcostbank/v1/users", userController.createUser);
//Modificacion usuario
app.put("/lowcostbank/v1/users/:id", userController.putUser);


//Login
app.post("/lowcostbank/v1/login", authController.login);
//Logout
app.post("/lowcostbank/v1/logout/:id", authController.doLogout);



//Consulta cuentas de un usuario
app.get("/lowcostbank/v1/users/:id/accounts", accountController.getAccounts);
//Detalle de la cuenta
app.get("/lowcostbank/v1/accounts/:id", accountController.getAccount);
//Alta de  cuenta
app.post("/lowcostbank/v1/users/:id/accounts", accountController.postAccount);
// Actualizar cuenta (saldo)
app.put("/lowcostbank/v1/accounts/:id", accountController.putAccount);
//Eliminar una cuenta
app.delete("/lowcostbank/v1/accounts/:id", accountController.deleteAccount);

//Consulta movimientos de una cuenta
app.get("/lowcostbank/v1/accounts/:id/movements", movementController.getMovementsbyAccount);
// Alta de movimientos
app.post("/lowcostbank/v1/accounts/:id/movements", movementController.postMovement);
// Modificar  movimiento
app.put("/lowcostbank/v1/movements/:id", movementController.putMovement);
//Consultar detalle de un movimiento
app.get("/lowcostbank/v1/movements/:id", movementController.getMovement);
//Consulta todos los movimientos
app.get("/lowcostbank/v1/movements", movementController.getMovements);
//Borrar un movimiento
app.put("/lowcostbank/v1/movements/:id", movementController.deleteMovement);

const crypt = require('../crypt');
const requestJson = require ('request-json');
const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edvcv/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


//consulta movimientos de una cuenta
function getMovementsbyAccount(req, res) {
 console.log("GET /lowcostbank/v1/accounts/:id/movements");
 var iban = req.params.id;
 var iban = iban.replace(/['"]+/g, '')
 console.log(iban);

 httpClient = requestJson.createClient(mlabBaseURL);
 var query = 'q={"iban":"' + iban + '"}';
 console.log("La query es " + query);

 httpClient.get("movement?" + query + "&" + mlabAPIKey,
   function(err, resMLab, body) {
         if (err) {
       response = {
         "msg" : "Error obteniendo movimientos de la cuenta"
       }
       res.status(500);
     } else {
       if (body.length > 0) {
         response = body;
       } else {
         response = ('la cuenta no tiene movimientos');
         res.status(204);
        //  res.status(404).send('La cuenta no tiene movimientos');
       }
     }
     res.send(response);
     console.log("response es :" + response);
   }
 );
}




//alta de movimiento
function postMovement(req, res) {
 console.log("POST /lowcostbank/v1/accounts/:id/movements");
 var query = 'q={"iban":"' + req.params.id + '"}';
 var iban = req.params.id;
 var amount = req.body.amount;
 var currency = req.body.currency;
 var detail = req.body.detail;
 var date = new Date().toJSON().slice(0,10);
 var sign =Math.sign(amount);

 if (sign==1) {
   var type = "Ingreso"
   console.log("El signo es ==1");
 } else if (sign==-1) {
   var type = "Reintegro"
   console.log("El signo es ==1");
 }else{
   console.log("El signo no está definido, amount incorrecta");
   var response = {
       "msg" : "El saldo del movimiento no puede ser 0"
   };
   res.status(400);
   res.send(response);
 }
 console.log("el tipo es " + type);
 var httpClient = requestJson.createClient(mlabBaseURL);


//Primero validamos que la cuenta exista y vemos el saldo
httpClient.get("account?" + query + "&" + mlabAPIKey,
  function(err, resMLab, body) {
    console.log("El iban  de la cuenta es:" + iban);
      if (err) {
        response = {"msg" : "Error obteniendo la cuenta."}
        res.status(500);
      } else {
        if (body.length > 0) {
          console.log("la longitud del body es " + body.length);
          currentbalance = body[0].balance;
          console.log("currentbalance es " + currentbalance);
          response = body;

//Despues consultamos los movimientos para añadir un identificador valido
        var query = 's={"movementId":-1}&l=1';
        console.log(query);
         httpClient.get("movement?" + query + '&' + mlabAPIKey,
           function(err, resMLab, body) {
              if (!err){
                  var id =body[0].movementId + 1
                  console.log("el nuevo id será: "+ id);
                  var newmovement = {
                    "movementId" : id,
                    "iban" : req.params.id,
                    "amount" : amount,
                    "currency" : currency,
                    "type" : type,
                    "date" : date,
                    "detail" : detail
                  }
                  console.log(newmovement);
           }else{
             res.send("Error al crear identificador de movimiento")
           };

//Damos de alta el movimiento
           httpClient.post("movement?" + mlabAPIKey, newmovement,
           function(err, resMLab, body){
             if (!err){
               console.log("movimiento creado con exito");
               res.status(201);
               res.send(body);
             }else{
               res.send("Error al crear el movimiento")
             };
           }
         );

//Actualizamos el saldo de la cuenta
      console.log("El iban  de la cuenta es: " + iban);
      var query = 'q={"iban":"' + iban + '"}';
      console.log("la query es  " + query);
      var newbalance = Number(currentbalance) + Number(amount);
      var put = '{"$set":{"balance": "'+ newbalance + '"}}';
      console.log("el put es " + put);
      var put = JSON.parse(put);
      console.log("el put parse es " + put);

          httpClient.put("account?" + query + '&' + mlabAPIKey, put,
            function(err, resMLab, body) {

            }
          )

       }
     )
      } else {
        //la cuenta no existe
        response = {"msg" : "Error de cuenta" };
        res.status(404);
        }
      }
    }
  );
}



//Modificar movimiento
function putMovement(req,res){
        console.log("PUT /lowcostbank/v1/movements/:id");
        var httpClient = requestJson.createClient(mlabBaseURL);
        var movementId = req.params.id;
        var query = 'q={"movementId":' + movementId + '}';
        var put = '{"$set":{"detail": "'+ req.body.detail+ '"}}';
        console.log(put);

           httpClient.put("movement?" + query + '&' + mlabAPIKey, JSON.parse(put),
             function(err, resMLab, body) {

               httpClient.get("movement?" + query + "&"+ mlabAPIKey,
                 function(err, resMLab, body){

                 if (err) {
                   var response = {
                     "msg" : "Error modificando el movimiento"
                   };
                   res.status(500);
                 }else {
                   if (body.length>0) {
                     var response = body[0];
                   }else {
                     //el movimiento no existe
                     var response = { "msg" : "Error al obtener movimiento"  };
                     res.status(404);
                   }
                 }
                 res.send(response);
                 }
               )
             }
           )
         }



//consulta todos los movimientos
function getMovements(req, res){
  console.log("GET /lowcostbank/v1/movements");

  var httpClient = requestJson.createClient(mlabBaseURL);

           httpClient.get("movement?" + mlabAPIKey,
             function(err, resMLab, body){
               var response =!err ?
               body : {"msg" : "Error obteniendo movimientos"}
               res.send(response);
             }
           )
         }



//consulta detalle de un movimiento
function getMovement(req, res) {
  console.log("GET /lowcostbank/v1/movements/:id");

          var movementId = req.params.id;
          var query = 'q={"movementId":' + movementId + '}';
          console.log("La query es " + query);

          httpClient = requestJson.createClient(mlabBaseURL);
          httpClient.get("movement?" + query + "&" + mlabAPIKey,
            function(err, resMLab, body) {
              console.log(body);
              if (err) {
                response = {"msg" : "Error obteniendo el movimiento."}
                res.status(500);
              } else {
                if (body.length > 0) {
                  response = body;
                } else {
                  response = {"msg" : "Error obteniendo detalle del movimiento" };
                  res.status(404);
                }
              }
              res.send(response);
            }
          );
         }



// eliminar movimiento
function deleteMovement(req, res) {
  console.log("DELETE /lowcostbank/v1/movements/:id");
      //consultamos el iban del movimiento de la cuenta
      var movementId = req.params.id;
      var query = 'q={"movementId":' + movementId + '}';
      console.log(query );

      httpClient = requestJson.createClient(mlabBaseURL);
      httpClient.get("movement?" + query + "&" + mlabAPIKey,
        function(err, resMLab, body) {
          console.log("consultamos el detalle del movimiento");
          if (err) {
            response = {"msg" : "Error obteniendo el movimiento."}
            res.status(500);
          } else {
            if (body.length > 0) {
              response = body;
              var iban = body[0].iban
              console.log(iban);

            } else {
              response = {"msg" : "Error obteniendo detalle del movimiento" };
              console.log("error en el detalle");
              res.status(404);
            }
          }
        //  res.send(response);
        }
      );

  //consultamos el saldo de la cuenta
        var query2 = 'q={"iban":' + iban + '}';
        console.log("la query2 es " + query2);
            httpClient.get("account?" + query2 + "&" + mlabAPIKey,
              function(err, resMLab, body) {
                console.log("estoy consultando la cuenta");
                if (err) {
                  response = {"msg" : "Error obteniendo la cuenta." }
                  res.status(500);
                } else {
                  if (body.length > 0) {
                    response = body;
                    currentbalance = body[0].balance;
                    console.log("currentbalance es " + currentbalance);
                  } else {
                    response = {
                      "msg" : "Error obteniendo detalle de la cuenta"
                    };
                    res.status(404);
                  }
                }
                res.send(response);
              }
            );



    //eliminamos el movimiento
      httpClient.put("movement?" + query + "&" + mlabAPIKey, JSON.parse("[]"),
     function (err, resMLa, body){
        if (!err)
            {
                var response = {"msg":"Movimiento borrado correctamente"};
                 //actualizamos el balance
                 console.log("El iban  de la cuenta es: " + iban);
                 var query = 'q={"iban":"' + iban + '"}';
                 console.log("la query es  " + query);
                 var newbalance = Number(currentbalance) - Number(amount);
                 var put = '{"$set":{"balance": "'+ newbalance + '"}}';
                 console.log("el put es " + put);
                 var put = JSON.parse(put);
                 console.log("el put parse es " + put);
                 httpClient.put("account?" + query + '&' + mlabAPIKey, put,
                 function(err, resMLab, body) {

                 }
               )
       }
       else
       {
         var response = {"msg":"Error al borrar el movimiento"};
         res.status(409);
         res.send(response);
       }
     }
   );
 }





module.exports.deleteMovement =deleteMovement;
module.exports.postMovement =postMovement;
module.exports.getMovementsbyAccount =getMovementsbyAccount;
module.exports.getMovements =getMovements;
module.exports.getMovement =getMovement;
module.exports.putMovement =putMovement;

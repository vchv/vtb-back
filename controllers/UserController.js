const crypt = require('../crypt');


const requestJson = require ('request-json');
const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edvcv/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function getUsers(req, res){
  console.log("entramos en el GET get/lowcostbank/v1/users");

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("cliente created");

  httpClient.get("user?" + mlabAPIKey,
    function(err, resMLab, body){
      var response =!err ?
      body : {"msg" : "Error obteniendo usuarios"}
      res.send(response);
    }
  )
}


function getUsersById(req, res){
  console.log("entramos en el GET get/lowcostbank/v1/users:id");
  var id = req.params.id;
  var query = 'q={"userId":' + id + '}';

  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("cliente created");
  console.log(mlabBaseURL);
  console.log("user?" + query + "&"+ mlabAPIKey);

  httpClient.get("user?" + query + "&"+ mlabAPIKey,
    function(err, resMLab, body){

    if (err) {
      var response = {
        "msg" : "Error obteniendo usuario"
      };
      res.status(500);
    }else {
      if (body.length>0) {
        var response = body[0];
      }else {
        var response = {
          //El usuario no existe
          "msg" : "Error de usuario"
        };
        res.status(404);
      }
    }
    res.send(response);
    }
  )
}




function createUser(req, res){
  console.log("entramos en el POST, añadiendo, post/lowcostbank/v1/users");
  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("client created");

  var query = 's={"userId":-1}&l=1';
  console.log(query);

  httpClient.get("user?" + query + "&"+ mlabAPIKey,
    function(err, resMLab, body){
      if (!err){
        console.log(body[0]);
           //var id = body.length +1
           var idNueva =body[0].userId + 1
           console.log(idNueva);

          var logged = false
          var newUser = {
            "userId" : idNueva,
            "first_name" : req.body.first_name,
            "last_name" : req.body.last_name,
            "email" : req.body.email,
            "password" : crypt.hash(req.body.password)
          }
    }else{
      res.send("Error al crear identificador de usuario")
    };
    httpClient.post("user?" + mlabAPIKey, newUser,
      function(err, resMLab, body){
        if (!err){
          res.send(body)
          console.log("usuario guardado con exito");
          res.status(201);
        }else{
          res.send("Error al crear el usuario")
        };
      }
    )

  }
  )
}



 //Modificar usuario
function putUser(req,res){
      console.log("PUT /lowcostbank/v1/users/:id");
      var httpClient = requestJson.createClient(mlabBaseURL);
      var userId = req.params.id;
      var userId = userId.replace(/['"]+/g, '');
      var query = 'q={"userId":' + userId + '}';
      var put = '{"$set":{"first_name":"'+ req.body.first_name + '","last_name":"'+req.body.last_name +'"}}';
      console.log(put);

                httpClient.put("user?" + query + '&' + mlabAPIKey, JSON.parse(put),
                  function(err, resMLab, body) {

                    httpClient.get("user?" + query + "&"+ mlabAPIKey,
                      function(err, resMLab, body){

                      if (err) {
                        var response = {
                          "msg" : "Error modificando usuario"
                        };
                        res.status(500);
                      }else {
                        if (body.length>0) {
                          var response = body[0];
                        }else {
                          var response = {
                            //El usuario no existe
                            "msg" : "Error de usuario."
                          };
                          res.status(404);
                        }
                      }
                      res.send(response);
                      }
                    )
                  }
                )
              }



module.exports.getUsers = getUsers;
module.exports.getUsersById =getUsersById;
module.exports.createUser = createUser;
module.exports.putUser =putUser;

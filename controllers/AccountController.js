//const io = require('../io');
// para la funcion post y otras
const crypt = require('../crypt');
const requestJson = require ('request-json');
const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edvcv/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


//alta de cuenta
function postAccount(req, res){
  console.log("POST, añadiendo, post/lowcostbank/v1/users/id:/accounts");
  console.log('id de usuario: ' + req.params.id);

  req.params.id = parseInt( req.params.id, 10 )
  var balance  = parseFloat( "0.00")
  var currency = "eur"
  console.log(balance);
  var alias ="cuenta nueva"
  var date = new Date().toJSON().slice(0,10)
  var year = date.substring(0,2)
  var month = date.substring(5,7)
  var day = date.substring(8,10)
  var timestamp = new Date()
  var hour = timestamp.getHours()
  var minute = timestamp.getMinutes()
  var second = timestamp.getSeconds()
  var fijo = "ES010182"
  var iban = fijo +year + month+ day + hour + minute + second
  var account = {
    "userId" : req.params.id,
    "iban" : iban,
    "balance" : balance,
    "currency" : currency,
    "alias" : alias
  };
  var httpClient = requestJson.createClient(mlabBaseURL);
  console.log("client created");

  httpClient.post("account?" + mlabAPIKey, account,
    function(err, resMLab, body){
    console.log("cuenta creada y asociado con exito");
    res.status(201);
    //res.send({"msg" : "cuenta creada y asociado con exito"});
    res.send(body);
    }
  )
}



//consulta cuenta de un usuario
function getAccounts(req, res) {
 console.log("GET /lowcostbank/v1/users/:id/accounts");

 var userId = req.params.id;
 var query = 'q={"userId":' + userId + '}';
 console.log("La query es " + query);

 httpClient = requestJson.createClient(mlabBaseURL);
 httpClient.get("account?" + query + "&" + mlabAPIKey,
   function(err, resMLab, body) {
     console.log(body);

     if (err) {
       response = {         "msg" : "Error obteniendo cuentas."       }
       res.status(500);
     } else {
       if (body.length > 0) {
         response = body;
         res.status(200);
       } else {
         //el usuario no existe o no tiene cuentas
         response = {           "msg" : "El cliente no tiene cuentas."         };
         res.status(404);
       }
     }
     res.send(response);
   }
 );
}


//consulta detalle de una cuenta
function getAccount(req, res) {
 console.log("GET /lowcostbank/v1/accounts/:id");

 var iban = req.params.id;
 var query = 'q={"iban":"' + iban + '"}';
 console.log("La query es " + query);

 httpClient = requestJson.createClient(mlabBaseURL);
 httpClient.get("account?" + query + "&" + mlabAPIKey,
   function(err, resMLab, body) {
     console.log(body);

     if (err) {
       response = {
         "msg" : "Error obteniendo la cuentaa."
       }
       res.status(500);
     } else {
       if (body.length > 0) {
         response = body;
         currentbalance = body[0].balance;
         console.log("currentbalance es " + currentbalance);
       } else {
         response = {
           "msg" : "Error obteniendo detalle de la cuenta"
         };
         res.status(404);
       }
     }
     res.send(response);
   }
 );
}


//Modificar cuenta
function putAccount(req,res){
 console.log("PUT /lowcostbank/v1/accounts/:id");

 var httpClient = requestJson.createClient(mlabBaseURL);
 console.log("Client created");

 var iban = req.params.id;
 console.log(iban);
 // var id = id.replace(/['"]+/g, '');
 // console.log(id);
 var query = 'q={"iban":"' + iban + '"}';
 console.log(query);
 var put = '{"$set":{"alias": "'+ req.body.alias+ '"}}';
 console.log(put);
 //put del estilo { '$set': { alias: 'cuenta para viaje' } }
 var put = JSON.parse(put);
 console.log(put);

           httpClient.put("account?" + query + '&' + mlabAPIKey, put,
             function(err, resMLab, body) {

               httpClient.get("account?" + query + "&"+ mlabAPIKey,
                 function(err, resMLab, body){

                 if (err) {
                   var response = {
                     "msg" : "Error modificando la cuenta"
                   };
                   res.status(500);
                 }else {
                   if (body.length>0) {
                     var response = body[0];
                   }else {
                     var response = {
                       //la cuenta no existe
                       "msg" : "Error con la cuenta"
                     };
                     res.status(404);
                   }
                 }
                 res.send(response);
                 }
               )
             }
           )
         }



//eliminar cuenta
function deleteAccount(req, res) {
  console.log("DELETE /lowcostbank/v1/accounts/:id");

          var iban = req.params.id;
          var query = 'q={"iban":"' + iban + '"}';
          console.log("La query es " + query);

          httpClient = requestJson.createClient(mlabBaseURL);
          httpClient.get("account?" + query + "&" + mlabAPIKey,
            function(err, resMLab, body) {
              console.log(body);
              //console.log(body[0].balance);

              if (err) {
                response = {"msg" : "Error obteniendo detalle de la cuenta." };
                res.status(500);
              } else {

                if (body[0].balance < 0) {
                  response = {"msg" : "No se puede cancelar una cuenta con balance negativo"};
                  res.status(404);
                } else {
                  console.log("La query es " + query);
                      httpClient.put("account?" + query + "&" + mlabAPIKey, JSON.parse("[]"),
                          function(err, resMLab, body){
                            var response =!err ?
                            body : {"msg" : "Error borrando cuenta"}
                            //res.send(response);
                          }
                        )
                        response = {"msg" : "Se ha realizado la cancelacion de la cuenta"};
                      }
                      res.send(response);
            }
          }
        );
      }







module.exports.deleteAccount =deleteAccount;
module.exports.putAccount =putAccount;
module.exports.getAccount =getAccount;
module.exports.getAccounts =getAccounts;
module.exports.postAccount =postAccount;

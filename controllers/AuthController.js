const crypt = require('../crypt');
const requestJson = require ('request-json');
const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8edvcv/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;



function login(req,res){
     console.log("POST /lowcostbank/v1/login")
     var encontrado = false;
     var query = 'q={"email":"' + req.body.email + '"}';
     var httpClient = requestJson.createClient(mlabBaseURL);

     httpClient.get("user?" + query + '&' + mlabAPIKey,
       function(err, resMLab, body) {
         if (err) {
           var response = {"msg" : "Error obteniendo usuario"};
           res.status(500);
         } else {
           if (body.length > 0) {
             console.log(body);
             var userHashPassword = body[0].password;
             console.log(userHashPassword);
             //reviso el password OK
             var resultado = crypt.checkpassword(req.body.password,userHashPassword)
             if (resultado===true){
               var response = {
                   "userId" : body[0].userId,
                   "first_name" : body[0].first_name,
                   "last_name" : body[0].last_name
               };
               var putBody = '{"$set":{"logged":true}}';
               var putBody2 = JSON.parse(putBody);

               httpClient.put("user?" + query + '&' + mlabAPIKey, JSON.parse(putBody),
                 function(err, resMLab, body) {
                   console.log("se añade la etiqueta logged al usuario logado");
                 }
               )

             }else{
               //contraseña erronea, pero no se puede indicar directamente por seguridad
               var response = {"msg" : "El usuario/contraseña no son correctos, Pruebe de nuevo." };
               res.status(401);
             }

           } else {
             //mail erroneo, pero no se puede indicar directamente por seguridad
               var response = {"msg" : "El usuario/contraseña no son correctos" };
               res.status(404);
           }
         }
         res.send(response);
       }
     )
   }






function doLogout(req,res){
     console.log("POST /lowcostbank/v1/logout/:id")
     var query = 'q={"userId":' + req.params.id + '}';
     var httpClient = requestJson.createClient(mlabBaseURL);

        httpClient.get("user?" + query + '&' + mlabAPIKey,

             function(err, resMLab, body) {
               if (err) {
                 var response = { "msg" : "Error obteniendo usuario"};
                 res.status(500);
               } else {
                 if (body.length > 0) {
                   var user =body[0];
                    if (user.logged) {
                      var putBody = '{"$unset":{"logged":""}}';
                      console.log(putBody);
                      httpClient.put("user?" + query + '&' + mlabAPIKey, JSON.parse(putBody),
                        function(err, resMLab, body) {
                          console.log("se quita la etiqueta logged al usuario logado, logout con exito");
                          res.send({"msg" : "Logout realizado con exito. Esperamos verle pronto de nuevo!"});
                        }
                      )
                   }else {
                     //Usuario no logado anteriormente
                     var response = {"msg" : "Error al realizar el logout. ¿Ha comprobado que esté logado?" };
                     res.send(response);
                   }

                 }else {
                   // El usuario no existe
                   var response = {"msg" : "Error con el usuario al realizar el logout" };
                   res.send(response);
                 }
                }
              }
            )
          }



module.exports.login = login;
module.exports.doLogout = doLogout;

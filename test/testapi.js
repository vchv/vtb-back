const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();
//para que no tenga que tener otra consola abierta
var server = require('../server');




describe ('Test de API de Usuarios',
  function(){
    it('Prueba de que la API de usuarios responde', function(done) {
      chai.request('http://localhost:3000')
      .get('/apitechu/v1/hello')
      .end(
        function(err, res){
          console.log("Request finished");
          res.should.have.status(200);
          console.log(res.body.msg);
          res.body.msg.should.be.eql("Hola desde API TechU!");
          done();
        }
      )
    }
  ),

  it('Prueba de que la API devuelve la lista de usuarios correcta', function(done) {
    chai.request('http://localhost:3000')
    .get('/lowcostbank/v1/users')
    .end(
      function(err, res){
        console.log("Consulta de usuarios finalizada");
        res.should.have.status(200);
        for (user of res.body.users){
          user.should.have.property("email");
          user.should.have.property("password");
        }

        done();
      }
    )
  }
)
  }
)

describe('Alta usuario',
  function() {

    var idUser = "";

    it('Alta usuario ok',
      function(done) {
          chai.request('http://localhost:3000')
          .post('/lowcostbank/v1/users')
          .set('content-type', 'application/json')
          .send({"email": "maria@perez.com",
                 "password": "12345678",
                 "name": "Maria",
                 "surname" : "Perez"
           })
          .end(
              function(err,res) {
                console.log("Test de alta usuario enviado");
                console.log('err' + err);
                console.log('body<' + res.body);
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.have.property('_id');
                res.body.msg.should.be.eql("Entramos en la peticion");
                console.log (res.body.id);
                idUser = res.body.id;
                //id_token = res.body.auth_token;
                console.log ('idUser --> ' + idUser );
                done();
              }
          )
      }
    )
  }
)

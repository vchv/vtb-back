#Dockerfile APITECHU

#Imagen raiz
FROM node

#carpeta trabajo
WORKDIR /apitechu

#Añado archivos de mi aplicacion a imagen
ADD . /apitechu

#Instalo los paquetes necesarios
RUN npm install

#Abrir puerto de la API
EXPOSE 3000

#comando de inicializacion
CMD ["npm", "start"]
